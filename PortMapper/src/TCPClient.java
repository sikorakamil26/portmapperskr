import java.io.*;
import java.net.*;
import java.util.Scanner;

public class TCPClient {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;


    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        System.out.println("Connection succesful! You can start communicating");
    }

    public String sendMessage(String msg) throws IOException {
        out.println(msg);
        out.flush();
        String resp = in.readLine();
        return resp;
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public static void main(String[] args) throws IOException {
        TCPClient client = new TCPClient();
        client.startConnection("127.0.0.1", 6666);
        Scanner in = new Scanner(System.in);
        String message;
        while((message = in.nextLine()) != "EXIT"){
            System.out.println(client.sendMessage(message));
        }
        client.stopConnection();
    }

}