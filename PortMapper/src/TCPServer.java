import java.io.*;
import java.net.*;
import java.util.HashMap;

public class TCPServer{

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private static HashMap<String, ConnectionData> portMapper = new HashMap<>();

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String inputLine;

        while((inputLine = in.readLine()) != "EXIT"){
            System.out.println(inputLine);
            String[] request = inputLine.split(" ");

            if (request[0].equals("REGISTER")) {
                try {
                    register(request[1], request[2], request[3]);
                    out.println("Success! Service IP port registered as " + request[1]);
                    out.flush();
                } catch (IndexOutOfBoundsException e) {
                    out.println("Could not register: wrong data format");
                    out.flush();
                } catch (Exception e) {
                    out.println("Couln nor register: wrong data provided");
                    out.flush();
                }
            }else if(request[0].equals("GET")){
                try{
                    String client = portMapper.get(request[1]).toString();
                    if(client != null) out.println(client);
                }catch (Exception e){
                    out.println("There is no port with that name");
                }
            }else if(request[0].equals("CALL")){
                TCPTestClient client = new TCPTestClient();
                ConnectionData clientData = portMapper.get(request[1]);
                client.startConnection(clientData.getIp(), clientData.getPort());
                out.println(client.sendMessage(request[2]));
                client.stopConnection();
            }else{
                out.println("No such commend :/ I can only receive: REGISTER, GET and CALL requests");
            }
        }
        stop();
        out.print("goodbye");
    }

    public static void register(String name, String ip, String port){
        try{
            portMapper.put(name, new ConnectionData(ip, Integer.parseInt(port)));
        }catch (Exception e){
            throw e;
        }
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();
    }

    public static void main(String[] args) throws IOException {
        TCPServer server = new TCPServer();
        server.start(6666);
    }
}